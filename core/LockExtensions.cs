using System;
using System.Threading;
using System.Threading.Tasks;

namespace core
{
	using core.io.slimLock;

	public static class LockExtensions
	{
		public static Task WaitAsync(this ISlimLock slim, Func<Task> handle) => slim.WaitAsync(handle, default);
		public static Task WaitAsync(this ISlimLock slim, Action handle) => slim.WaitAsync(handle, default);

		public static async Task WaitAsync(this ISlimLock slim, Action handle, CancellationToken token)
		{
			await slim.WaitAsync(token);
			try
			{
				handle();
			}
			finally
			{
				slim.Release();
			}
		}
		
		public static async Task WaitAsync(this ISlimLock slim, Func<Task> handle, CancellationToken token)
		{
			await slim.WaitAsync(token);
			try
			{
				await handle();
			}
			finally
			{
				slim.Release();
			}
		}
	}
}
