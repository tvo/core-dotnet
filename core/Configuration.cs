using Microsoft.Extensions.Configuration;

namespace core
{
	public static class Configuration
	{
		public static IConfiguration BuildConfiguration()
		{
			var configBuilder = new ConfigurationBuilder();
			configBuilder.AddJsonFile("appsettings.json");
			configBuilder.AddEnvironmentVariables();
			return configBuilder.Build();
		}
	}
}
