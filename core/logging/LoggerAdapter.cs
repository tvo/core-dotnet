using Microsoft.Extensions.Logging;
using Immutability;

namespace core.logging
{
	[Immutable]
	public class LoggerAdapter<T> : ILoggerAdapter<T>, ILoggerAdapter
	{
		private readonly ILogger logger;
		public LoggerAdapter(ILoggerFactory factory)
		{
			logger = factory.CreateLogger(typeof(T));
		}

		public void LogTrace(string message) => logger.LogDebug(message);
		public void LogDebug(string message) => logger.LogDebug(message);
		public void LogInformation(string message) => logger.LogInformation(message);
		public void LogWarning(string message) => logger.LogWarning(message);
		public void LogError(string message) => logger.LogError(message);

		public void LogTrace(string message, params object[] args) => logger.LogDebug(message, args);
		public void LogDebug(string message, params object[] args) => logger.LogDebug(message, args);
		public void LogInformation(string message, params object[] args) => logger.LogInformation(message, args);
		public void LogWarning(string message, params object[] args) => logger.LogWarning(message, args);
		public void LogError(string message, params object[] args) => logger.LogError(message, args);
	}
}
