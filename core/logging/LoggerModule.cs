using System;
using Autofac;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Autofac.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace core.logging
{
	using rx;

	public class LoggerModule : Module
	{
		private readonly IConfiguration config;

		public LoggerModule(IConfiguration config)
		{
			this.config = config;
		}

		protected override void Load(ContainerBuilder builder)
		{
			var logConfig = new LoggerConfiguration()
				.MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
				.ReadFrom.Configuration(this.config)
				;

			builder.RegisterSerilog(logConfig);

			builder.RegisterGeneric(typeof(LoggerAdapter<>)).As(typeof(ILoggerAdapter<>));
			
			// configure static logger.
			builder.RegisterBuildCallback(scope => {
				var logger = scope.Resolve<ILoggerAdapter<ObservableTrace.TraceObservable<object>>>();
				ObservableTrace.ConfigureLogger(logger);
			});
		}
	}
}
