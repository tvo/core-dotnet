using System;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace core.webSockets
{
	public class WebSocketModule : Module
	{
		private readonly IConfiguration config;

		public WebSocketModule(IConfiguration config)
		{
			this.config = config;
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterModule(new core.pubSub.PubSubModule(config));

			builder.RegisterType<SocketConnection>();
		}
	}
}
