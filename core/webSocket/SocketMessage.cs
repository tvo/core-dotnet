

namespace core.webSockets
{
	public class SocketMessage
	{
		public readonly string Message;

		public SocketMessage(string message)
		{
			Message = message;
		}
	}
}
