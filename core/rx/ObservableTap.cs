using System;
using System.Reactive.Linq;

namespace core.rx
{
	public static class ObservableTap
	{
		public static IObservable<T> Tap<T>(this IObservable<T> source, Action<T> act) => source.Select(x => { act(x); return x;});
	}
}
