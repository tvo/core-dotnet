using System;
using System.Linq;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace core.rx
{
	public class Broadcast<T> : IObservable<T>
	{
		private readonly List<IObserver<T>> subscribers = new List<IObserver<T>>();
		private readonly IObservable<T> UpStream;
		private IDisposable UpStreamSub = null;

		public Broadcast(IObservable<T> upStream)
		{
			UpStream = upStream;
		}

		private void Add(IObserver<T> sub)
		{
			subscribers.Add(sub);

			if(subscribers.Count == 1)
			{
				UpStreamSub = UpStream.Subscribe(next => {
					foreach(var s in subscribers.ToList()) // to list because subscribers could be modified.
						s.OnNext(next);
				}, error => {
					foreach(var s in subscribers.ToList())
						s.OnError(error);
				}, () => {
					foreach(var s in subscribers.ToList())
						Remove(s);
				});
			}
		}

		private void Remove(IObserver<T> sub)
		{
			sub.OnCompleted();
			if(subscribers.Contains(sub))
				subscribers.Remove(sub);
			if(subscribers.Count == 0 && UpStreamSub != null)
			{
				UpStreamSub.Dispose();
				UpStreamSub = null;
			}
		}

		public IDisposable Subscribe(IObserver<T> source)
		{
			Add(source);
			return Disposable.Create(() => {
				Remove(source);
			});
		}
	}

	// this is need just for the observable extension.
	public static class MyReactiveExtensions
	{
		public static IObservable<T> Broadcast<T>(this IObservable<T> source) => new Broadcast<T>(source);
	}
}
